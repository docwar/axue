import { defineConfig} from 'vitepress'

const 中文={
  label: '简体中文',
  lang: 'zh',   //比zh-CN更通用，后者代表中国大陆
  link: '/zh/'
}
const english={
  label: 'English',
  lang: 'en', 
  link: '/en/'
}
const defaultRoot=中文
const defaultRootPath = defaultRoot.link


// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "傲雪",
  description: "面向pc端、跨框架的浏览器原生组件库，支持在业务开发层自定义标签、图标、样式，开创loadtime范式先河。",
  head: [
    // 添加网站图标
    ['link', { rel: 'icon', href: "/favicon.ico" }],   //有bug，注意拷贝
    //分析脚本
    [
      'script',
      { async: '', src: 'https://www.googletagmanager.com/gtag/js?id=G-VJBGXP59P3' }
    ], 
    [
      'script',
      {},
      `window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'G-VJBGXP59P3');`
    ]
  ],
  srcDir: 'src', //markdown源码,后面的路径不要带src，不要加/或./，框架会拼接
  outDir: 'public',  //gitlabpages只能发布public目录，注意git要忽略
  assetsDir: 'static',    //outDir目录下的子目录，但指编译出的静态文件，不包括ico、logo，需要ci特别cp
  locales: {
    root:defaultRoot,   //默认配置无需重复写在下面，但不能没有
    en: english         
  },
  themeConfig: { 
    logo: '/logo.png',   //有bug，注意拷贝
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: '指南', link: `${defaultRootPath}/guide/why` },
      { text: '组件能力', link: `${defaultRootPath}/widgets/index` },
      { text: '自定义能力', link: `${defaultRootPath}/config/index` }
    ],
    // footer: {
    //   message: '',
    //   copyright: 'Copyright © 2024-present 青锋三尺，樵夫十年'
    // },
    sidebar: [
      {
        text: '快速开始',
        // collapsed:false,   //禁止折叠
        items: [
          { text: '为什么需要傲雪？', link: `${defaultRootPath}/guide/why` },
          { text: '快速开始', link: `${defaultRootPath}/guide/quickStart` },
          { text: '开发指引', link: `${defaultRootPath}/guide/devGuide` },
          // { text: '属性', link: `${defaultRootPath}/guide/question` },
          // { text: '事件', link: `${defaultRootPath}/guide/question` },
          // { text: '数据', link: `${defaultRootPath}/guide/question` },
          // { text: '不同方式的开发范例', link: `${defaultRootPath}/guide/question` },
          // { text: '故障排查', link: `${defaultRootPath}/guide/question` },
           { text: '版本计划', link: `${defaultRootPath}/guide/versionPlan` },
        ]
      },
      {
        text: '接口能力',
        // collapsed:false,
        // items: [
        //   { text: 'API调用指南', link: `${defaultRootPath}/API/apiGuide` },
        //   { text: '层级和交互模型', link: `${defaultRootPath}/API/zIndex` },
        //   { text: 'showTip', link: `${defaultRootPath}/API/showTip` },
        //   { text: 'showToast', link: `${defaultRootPath}/API/showToast` },
        //   { text: 'showSlot', link: `${defaultRootPath}/API/showSlot` },
        //   { text: 'showPage', link: `${defaultRootPath}/API/showPage` },
        //   { text: 'showModule', link: `${defaultRootPath}/API/showModule` },
        //   { text: 'showPropertyBar', link: `${defaultRootPath}/API/showPropertyBar` },
        //   { text: '节点工具', link: `${defaultRootPath}/API/node` },
        // ]
        items: [
          { text: 'API调用指南', link: `${defaultRootPath}/API/apiGuide` },
          {
            text: '弹框API',
            collapsed:false,
            items: [
              { text: '层级和交互模型', link: `${defaultRootPath}/API/zIndex` },
              { text: 'showTip', link: `${defaultRootPath}/API/showTip` },
              { text: 'showToast', link: `${defaultRootPath}/API/showToast` },
              { text: 'showSlot', link: `${defaultRootPath}/API/showSlot` },
              { text: 'showPage', link: `${defaultRootPath}/API/showPage` },
              { text: 'showModule', link: `${defaultRootPath}/API/showModule` },
              { text: 'showPropertyBar', link: `${defaultRootPath}/API/showPropertyBar` },
            ]
          },
          {
            text: '节点API',
            collapsed:false,
            items: [
              // { text: '节点处理原则', link: `${defaultRootPath}/API/node` },
            ]
          },
        ]
      },
      {
        text: '组件能力',
        // collapsed:false,
        items: [
          { text: '组件使用指南', link: `${defaultRootPath}/widgets/index` },
          { text: '组件预览', link: `${defaultRootPath}/widgets/preview` },
          {
            text: 'atom域',
            collapsed:false,
            items: [
              { text: 'switch{开关}', link: `${defaultRootPath}/widgets/atom/switch` },
            ]
          },
          {
            text: 'base域',
            collapsed:false,
            items: [
              { text: 'logo-close{顶条}', link: `${defaultRootPath}/widgets/base/logo-close` },
              { text: 'high-light{高亮}', link: `${defaultRootPath}/widgets/base/high-light` },

              { text: 'min-menu{简易菜单}', link: `${defaultRootPath}/widgets/base/min-menu` },
              { text: 'menu-y2{二级菜单}', link: `${defaultRootPath}/widgets/base/menu-y2` },
              { text: 'tab-x{标签导航}', link: `${defaultRootPath}/widgets/base/tab-x` },

              { text: 'label-input{标签输入}', link: `${defaultRootPath}/widgets/base/label-input` },
              { text: 'label-select{标签选择}', link: `${defaultRootPath}/widgets/base/label-select`},
              { text: 'label-content{标签内容}', link: `${defaultRootPath}/widgets/base/label-content` },
              { text: 'label-content-edit{可编辑标签内容}', link: `${defaultRootPath}/widgets/base/label-content-edit` },
              { text: 'label-button-2{按钮行}', link: `${defaultRootPath}/widgets/base/label-button-2` },
            ]
          },
          {
            text: 'container域',
            collapsed:false,
            items: [
              { text: '布局系列组件<sup>layout</sup>', link: `${defaultRootPath}/widgets/container/layout` },
              { text: 'data-list-manager{数据生命周期管理}', link: `${defaultRootPath}/widgets/container/data-list-manager` }
            ]
          },
          // {
          //   text: 'business域',
          //   collapsed:false,
          //   items: [
          //     { text: 'button-menu-y2{按钮菜单}', link: `${defaultRootPath}/widgets/container/button-menu-y2` },
          //     { text: 'avatar-menu-y2{头像菜单}', link: `${defaultRootPath}/widgets/container/avatar-menu-y2` }
          //   ]
          // },
        ]
      },
      {
        text: '自定义能力',
        // collapsed:false,
        items: [
          { text: '自定义指南', link: `${defaultRootPath}/config/index` },
          { text: '自定义标签', link: `${defaultRootPath}/config/tag` },
          { text: '自定义默认图标', link: `${defaultRootPath}/config/icon` },
          { text: '自定义全局样式', link: `${defaultRootPath}/config/style`},
        ]
      }

    ],

    // socialLinks: [
    //   { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    // ]
  }
})
