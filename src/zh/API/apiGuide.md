# API调用体验

跟组件一样，API的调用体验，同样支持了一处导入，处处直接调用的体验。



### 提供几种体验

导入模块方法的方式是最通用的，直接调用未必支持了所以API，主要还是面向弹框API这样最常用的能力。

##### 常规体验

在每一个模块导入所需方法

```JavaScript

import {showTip} from "axue";
showTip.send('Hello, axue!');  
```

或导入所有API

```JavaScript
import * as axueUtil from "axue";
axueUtil.showTip.send('Hello, axue!');  
```

##### 直接调用

懒人可以省略导入步骤

在入口模块进行先行`import "axue"`导入框架，然后任意模块中都可以像使用标签一样使用API。但考虑到命名污染问题，框架侧肯定不会将每一个方法都注册全局变量，而是严格遵循axue前缀约束，将所有常用API挂载在`globalThis.axueAPI`这个全局变量上。

```JavaScript
axueAPI.showTip.send('Hello, axue!');  
```

##### 进一步封装

更懒的人看不惯...多级风格的调用语句

`axueAPI.showTip.send`这样的调用形式太长了对不对？理想的情况能否直接`showTip.send()` 甚至 `sendTip()` ？但这需要开发者自行把握应用会不会存在命名冲突。如果不存在，完全可以额外封装重新注册，以简化体验。

```JavaScript
/** 
* wrapAxueAPI.js
*/
//可以简化一层
globalThis.showTip = axueAPI.showTip
//或者再简化一层
globalThis.sendTip = axueAPI.showTip.send


//在入口模块处，import "axue"之后，import "./wrapAxueAPI.js"

```

甚至，你还可以进一步封装，这都取决于你的需求。

