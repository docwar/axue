# 模态提示框(showToast)

最常见的人机交互弹框

### 场景和形态

弹框选择之一，特点是需要用户确认才关闭、异步回调、有模态。一般用于提示确认。

![img](./assets/1704435987511-42.png)

### 使用方式

##### 调起模态框

```JavaScript
//导入
import {showToast} from "axue";

//定义插槽节点
const node = document.getElementById("usetomodal")

/**
    除了文本信息，默认只显示一个确认按钮，监听回调
    由于插槽可能要求弹框不要关闭，因此，点击button默认不会自动关闭弹框
*/
showToast.send({
    title: "axue欢迎你!",
    slot:  node,   
    confirmText:"知道了",
    onConfirm:function (){
        console.log("用户点击确认按钮")
        showToast.close(）       //主动关闭弹框
    } ,    
    
    isShowCancel:false, 
    cancelText:"不去",
    onCancel:cancel,      
})
function cancel(){
    console.log("用户点击取消")
    showToast.close(）
}
```

##### 主动关闭

注意，showToast要求开发者主动调用关闭接口

```JavaScript
/**
    关闭场景：
    1. 从外围逻辑或者slot内部主动关闭
    2. confirm和cancel按钮不会自动调用，主要为了保持最高的自由度
*/
showToast.close(）
```

### 参数项

| **内联属性** | **说明**                             | **类型**             | **默认值** |
| ------------ | ------------------------------------ | -------------------- | ---------- |
| title        | 提示标题                             | string               | ""         |
| message      | 提示文本，最多320字符                | string               | ""         |
| slot         | 插槽内容，高级自定义能力             | htmlNode或模板字符串 | null       |
| confirmText  | 确认按钮文本                         | string               | "确认"     |
| onConfirm    | 用户点击确认按钮的回调               | function             | null       |
| isShowCancel | 是否显示取消按钮，默认只显示确认按钮 | bool                 | false      |
| cancelText   | 取消按钮文本                         | string               | "取消"     |
| onCancel     | 用户点击取消按钮的回调               | function             | null       |

##### 其他说明

- slot最终会插入在message和buttons之间
- 作为不默认支持自动关闭的补偿，用户右击可以快捷退出模态