# 子模块(showModule)

这是自由度最高的组件之一，除了顶条，显示的内容完全由开发者决定。

### 场景和形态

大型应用构建，所依赖的弹框能力，提供快捷的交互体验，无模态、多实例，特点是：

1. 默认弹出位置跟随鼠标点击位置智能计算
2. 后弹出的子模块层级更高
3. 相比showPage的固定居中，宽高按比例自适应，子模块支持任意拖动，由自己定义宽度

![img](./assets/showModule.png)

### 使用方式

##### 弹出用法

比如我们要弹出一个单位查看器界面

```JavaScript
//导入
import {showModule} from "axue";

//构建调用参数对象
const node = document.getElementById("unitViewer")
let  args = {
    slot: node,
}

//在被点击元素的悬停、点击事件回调中，调用弹框
showModule.send(args)    
```

##### 自动关闭
需要点击子模块内置的close图标，将自动关闭弹框，并会额外支持关闭前进行一次回调。

##### 主动关闭
主动接口关闭时，不会回调。
```JavaScript
/** showModule.send方法实际上会返回一个uuid，以data-key的属性形式存在，可以使用该值来指向关闭
 *  如果传入的key不存在，则会被忽略
 */ 
showModule.close(key)    
```



### 参数项

| **内联属性**   | **说明**                          | **类型**             | **默认值** |
| -------------- | --------------------------------- | -------------------- | ---------- |
| name           | 子模块的名称                    | string | ""       |
| slot           | 可插入任意节点                    | htmlNode或模板字符串 | null       |
| isHiddenBorder | 是否隐藏边框，默认有边框          | bool                 | false      |
| backgroundColor        | 背景色，16进制颜色字符串，可以带alpha通道              | string             | #eeee       |
| width        | 宽度             | string             | 500px       |
| zindex         | 有效值在100~199之间，一般无需指定。什么情况需要指定呢？就是你认为默认的后弹层级越高的规则需要被打破 | number               | 100        |
| onClose        | 关闭前的回调，无回参              | function             | null       |


##### 其他说明

* send行为是触发实例的创建，close行为会触发对实例的即时销毁。
* 即便没有使用backgroundColor参数处理透明度时，默认值依然带了轻微的透明度，是否需要slot内部的节点样式保持和默认的"#eee"一致，取决于体验设计
