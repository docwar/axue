# 小提示（showTip）

顶层小提示，生命周期一般最短，层级最高。

### 场景和形态

弹框选择之一，特点轻量级、自动关闭无需交互无回调，无模态。 支持warning、info、error、success标准组合。

![img](./assets/1704435963859-39.png)

### 使用方式

##### 发送tip消息

```JavaScript
import {showTip} from "axue";


/**
    默认1.5秒后自动消失
    默认不指定提示类型，
*/
showTip.send('Hello, axue!');  
```

##### 指定提示类型

showTip.send，左边默认显示logo图标，业务含义不清晰，也可以指定类型   

```JavaScript
showTip.info('Hello, axue!');    //普通提示图标
showTip.warning('警告, axue!');  //警告图标
showTip.error('错误, axue!');    //错误图标
showTip.success('成功, axue!');  //成功图标
```

##### 提前关闭tip

```JavaScript
/**
    不等内部逻辑自动关闭，而是从外围主动关闭
    实际上，.info、.warning等方法会在默认延迟后自动调用.close()
*/
showTip.close(); 
```

### 高级配置

如果默认的样式背景、显示时长不能满足需求，可以使用配置能力

##### 使用方法

```JavaScript
import * as axueUtil from "axue";

//在调用前，进行全局配置，对后续所有类型的tip均有效
axueUtil.showTip.config({
    duration:1000,
    backgroundColor:"#eee"
})

//发送提示，此时将变成1s后自动关闭
axueUtil.showTip.info('Hello, axue!'); 
```

##### 全局配置项

| **内联属性**    | **说明**                       | **类型** | **默认值**  |
| --------------- | ------------------------------ | -------- | ----------- |
| duration        | 弹出到自动消失的时间间隔毫秒数 | string   | 1500        |
| backgroundColor | 背景色，符合16进制颜色字符串   | string   | "#ffffffee" |