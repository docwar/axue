# 跟随自定义内容(showSlot)

这是自由度最高的组件，显示的内容完全由开发者决定。

### 场景和形态

弹框选择之一，提供快捷的交互体验，无模态，特点是：弹出位置跟随鼠标点击位置智能计算。

![img](./assets/1704436012080-45.png)

### 使用方式

##### 弹出用法

比如我们要弹出一个单位查看器界面

```JavaScript
//导入
import {showSlot} from "axue";

//构建调用参数对象
const node = document.getElementById("unitViewer")
let  args = {
    slot: node,
    isHiddenBorder:true
}

//在被点击元素的悬停、点击事件回调中，调用弹框
showSlot.send(args)    
```

##### 自动关闭

点击弹框外围，将自动关闭弹框。

##### 主动关闭

借助点击弹框内部元素来处理逻辑，则需要主动调用接口关闭

```JavaScript
showSlot.close()
```

### 参数项

| **内联属性**   | **说明**                 | **类型**             | **默认值** |
| -------------- | ------------------------ | -------------------- | ---------- |
| slot           | 可插入任意节点           | htmlNode或模板字符串 | null       |
| isHiddenBorder | 是否隐藏边框，默认有边框 | bool                 | false      |

