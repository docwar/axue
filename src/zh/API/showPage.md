# 模态子页面(showPage)

介于showToast和showSlot之间的弹框，拥有前者的模态能力和后者的自由度优势。

### 场景和形态

弹框选择之一，用于在窗口中央显示子页面，比如快捷打开编辑器、查看详情等等。你可以嵌入构建任意结构的页面内容，兼具showToast和showSlot的优势。

![img](./assets/1704436028745-48.png)

### 使用方式

##### 调起模态框

```JavaScript
//导入
import {showPage} from "axue";
const node = document.getElementById("childPage")

//调起页面
showPage.send({
    slot:  node, 
    isHiddenBorder:true      
})
```

##### 主动关闭

在插槽内部的事件回调逻辑中，处理子页面关闭

```JavaScript
showPage.close(）
```

### 参数

| **内联属性**   | **说明**                 | **类型**             | **默认值** |
| -------------- | ------------------------ | -------------------- | ---------- |
| slot           | 可插入任意节点           | htmlNode或模板字符串 | null       |
| isHiddenBorder | 是否隐藏边框，默认有边框 | bool                 | false      |

##### 其他说明

- 右击可快捷退出子页面，也就是说，不应该在插槽内容中引导用户使用右击