# 层级和交互模型

理解框架的设计原则，有助于快速记忆和感知边界。以下是一些刻意的设计细节：

* 所有弹框类能力，均使用show前缀，这是实际的接口定义规范。
* 文档边栏列表，API的显示顺序与层级模型是对应的，比如showTip显示在最上层，showPropertyBar则显示在下层

### 层级模型

层级模型是基于交互体验设计，一定程度上也跟生命周期长短预期正相关，可以支持绝大多数场景的自然感。

![image-20240105142455433](./assets/zindex.png)

### 为什么存在层级模型？

一般而言，弹出层的生命周期很短，但并不绝对。当弹出层众多、且没有销毁的时候，我们需要清晰的知道每一个弹框API所对应的层级，以避免出现父弹出层弹出子层，子层却显示在父层之下的局面：

1. 体验很奇怪
2. 如果恰好父层的宽高比子层大，又是模态状态，则子层就被完全掩盖了



### 层级模型的特点

除了弹框API，二级菜单的特性设计也适用于层次模型的机制约束。

##### 单实例现象

几乎所有的弹窗API，以及二级菜单这种涉及弹框交互的组件，都建立在内置组件（不对外开放）的基础上，内置组件的容器是框架级定义的、单实例的，这意味着，每一个层级的容器处于共享状态。

映射到体验侧，这是什么概念？

* 比如对于showTip，默认1.5s自动关闭，但如果第一个还没关闭，只有0.5s，第二个调用逻辑就来了，则第一个tip会立即被销毁。
* 再比如对于showToast，你在下一步button按钮的回调逻辑中，或者插槽节点的事件逻辑里，重新调用新的showToast，则并不天然提供类似navigationToBack的体验，因为上一个弹框界面已经被替换式销毁了。如果确实需要呈现返回式体验，你可以借助全局变量对每一步进行状态快照，然后重新传参、弹出，来模拟这样的体验。
* menu菜单、showPage、showSlot、showPropertyBar.....均是如此特点，这是正常体验设计的一部分



##### 多实例现象

单实例让在任意一个时间切面，往往只有1个同类弹框在显示状态。但总有一些场景不能满足，那怎么办？

showModule是一个例外，他拥有showSlot的跟随鼠标能力，但事件机制、实例机制相反，一方面，需要内置的顶条close来关闭，不会监听外围点击，且可自由拖动，另一方面，并且不共享实例，在隐性业务层所限定的范围内，遵循先显示的子模块在下层，后显示的在上层的规律，以支持海量的弹层模块。

想象一个用户交互场景：

1. 点击菜单，弹出二级菜单
2. 点击二级菜单，弹出属性栏，同时二级菜单隐藏
3. 点击属性栏，弹出单位管理器（隐藏模块实例）
4. 点击单位管理器
   1. 弹出武器管理器（隐藏模块实例）
   2. 弹出任务管理器（隐藏模块实例）
   3. ......

5. 点击武器管理器，进入新的单位管理器（新的隐藏模块实例）
6. ......

从想象场景里，你发现了定位差异了没有，子模块在技术上是多实例，那是因为他在业务上恰恰应该是单例性质。
这其实也是作者对于大型桌面前端应用开发的一些理解，你在一个应用里，往往只需要存在一个业务意义上通用的某某对象管理器，而其底层维护的内存数据也应该是统一封装的。因此，理解showModule和其他show系列弹框API的定位差异，思考以什么样的设计模式开发子模块，对于新手而言，无论最终是否使用axue来开发业务，多少会一定帮助。