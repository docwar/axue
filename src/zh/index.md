---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "axue"
  text: "面向pc端的原生组件库"
  tagline: 支持在业务代码层定义框架的内置图标、样式、标签
  actions:
    - theme: brand
      text: 快速开始
      link: /zh/guide/quickStart
    - theme: alt
      text: 组件
      link: /zh/widgets/index

features:
  - title: 跨框架
    details: 统一风格的前提，无需牺牲团队的技术栈自由度，vue、react等随意混搭
  - title: 史上最小尺寸
    details: 200kb左右！剪除一切非必要封装，几无依赖，只需一处全量导入，处处任意使用
  - title: 内置常用API
    details: showTip、showToast、showSlot、showPage...等各类弹框能力，以及节点处理能力
  - title: loadtime新范式
    details: 框架的内置图标、样式、甚至向浏览器注册的自定义标签名，均可在业务代码加载、运行阶段进行自定义覆盖
---

