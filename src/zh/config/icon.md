# 自定义图标

需要理解两个基本概念：

* `[图标名称]`
* `[图标路径]`

### 覆盖默认图标

组件涉及默认图标的，均被统一管理，均可被覆盖。原则上，可以通过覆盖默认图标来全局定义的组件形态的，就不应该使用实例参数来定义。

##### customIcon

覆盖单个默认图标：

* customIcon是一个object，key为图标名称，value为新图标的运行时路径
* 只覆盖指定的默认图标，其余保持使用对应的内置图标

```JavaScript
import logo from "./demoUI/logo.png"
let config ={
     customIcon:{     
      	logo,
     }
     // 其他配置项...
};
export default config

```



### 内置图标大全



```JavaScript
export const axueDefaultIcon =  {
      logo,             //logo图标，该图标是最需要被全局覆盖的
    
      avatar,           //默认头像
      close,            //×
      unfold,           //向下展开，默认用
      fold,             //向上收缩，展开之后用
      right,            //右边，表示进入
      at,               //@形状
      add,              //加号
      search,           //搜索号
      threePoint,       //省略号
    
      typeError,        //错误
      typeWarning,      //警告
      typeSuccess,      //成功
      typeInfo,         //普通提示
      loading,          //加载提示
    
      //......
}

```



