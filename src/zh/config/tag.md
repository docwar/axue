# 自定义标签



需要区分三个基础概念：

1. `[底层标签名]`如`<logo-close>`、`<switch>`
2. `[默认标签名]`如`<axue-logo-close>`、`<axue-switch>`
3. `[最终标签名]`如`<i-love-you-logo-close>`、`<kai-guang>`



### 修改标签名

默认注册的自定义标签以`axue-`前缀开场，防止命名污染、以及确保自定义标签带有二分横岗。自定义标签名可能很鸡肋，但针对某些场景，不失为一个高级能力，如支撑品牌认知封装、反直觉命名修复、糊弄老板工作量等等。

##### customTagPrefix

统一修改标签前缀：

* customTagPrefix是string类型
* 影响所有实际使用的自定义组件，将“axue”前缀替换成自定义的前缀
* 字符串无需带`-`，不允许为空

以下的范例是，将所有的标签名前缀替换成'qingqiao'

```JavaScript
let config={
    customTagPrefix:"qingqiao",    //string类型
     // 其他配置项...
}    
export default config
```

##### customTag

修改单个组件：

* customTag是无嵌套的object类型，key、value均为string类型
* 只影响指定的底层标签，其余的使用默认标签名
* key使用`底层标签名`，不要带前缀
* value使用的是`最终标签名`

```JavaScript
let config={
    customTag:{           //为单个组件指定标签名，优先级最高，不会被自定义前缀覆盖
      'label-input': "label-input-haha",  //key去前缀，value是完整标签名
    },
     // 其他配置项...
}    
```



### 优先级

如果同时使用了customTagPrefix和customTag，会是什么结果？

```JavaScript
let config={
    customTagPrefix:"qingqiao",
    customTag:{       
      'label-input': "label-input-haha",
    },
     // 其他配置项...
}    
```

秉持个体优先于集体的原则，框架会先计算前缀，然后在计算结果的基础上，再计算最终标签名才正式向浏览器页面进行注册。换而言之，第一次计算，会将默认的`<axue-label-input>`标签切换成`<qingqiao-label-input>`，随后第二次计算又会将`<qingqiao-label-input>`切换成`<label-input-haha>`

因此，接下来的开发中，应该使用如下名称的自定义标签

* 标签输入`<label-input-haha>`
* 标签选项框`<qiangqiao-label-select>`
* 标签内容`<qiangqiao-label-content>`
* ......

