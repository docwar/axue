# 开发指引

以下是基本的注意事项

### 命名污染

框架以最小化原则占用了少量必要的全局变量，主要以axue开头。请尽量避免使用该字符开头的全局变量、以及节点，以免无意中污染了命名空间。

### 传参原则

##### .式传参

推荐使用lit进行业务开发，可优先使用.式传参，有两大优势：

- 不需要正反序列化
- 天然动态绑定，自动更新渲染机制。
- 有布尔类型

但是注意：只能自动监听整个对象的变动，不能监听子属性的变动，后者需要手动刷新。

```JavaScript
//可以使用this.requestUpdate();手动刷新
const yourComponent = document.getElementById('your-component-id');

if (yourComponent instanceof LitElement) {
  yourComponent.requestUpdate();
}

//也可以直接使用this.args=刷新数据，因为直接修改引用。
```

##### 原生传参

使用原生模版字符串，只能进行傻式传参，必须序列化处理。如`<my-ele args='${JSON.stringify(args)}'>`

原生传参也有优势：

————当一个通用属性，比如title、id、name，并没有被傲雪组件作为属性API定义时，可以直接使用原生写法传入，但这种本质上已经不是给组件内部传参了，而是直接写在自定义标签这个根上。

##### 关于布尔类型处理

你可能在一个lit组件中，既使用.式传参，也使用傻式传参。对于bool类型的传参要特别注意。原生传参基于字符串机制，通常使用"none"表示false，如果参数变量值时是undefined、null，那么布尔类型的属性会强制将其转换成"undefined"、"null"，从而导致被错误的视为true。



### 插槽规范

需要注意，一般需要使用slot=name的方式来告诉浏览器这是要插入到指定插槽的，否则可能会被视为普通元素。浏览器的机制是先将内嵌的div归入影子根的兄弟节点，也就是light Dom，然后再在内部通过引用指针指向插槽节点。

##### 布局容器传参

布局容器采用最简化的体验设计

```HTML
<!--组件内部定义-->
<slot></slot>

<!--直接卡入内容结构-->
<my-component>
      直接卡入插槽内容
</my-component>
```

##### 其他组件插槽

如果没有特别说明，只有一个插槽的情况下，默认插槽name为"slot"，既需要将示例的xxxname替换成slot

```HTML
<!--组件内部定义-->
<slot name="xxxname"></slot>


<!--使用slot=name指定位置-->
<my-component>
  <div slot="xxxname">需要用带slot属性的div标签包裹插槽内容</div>
</my-component>
```

##### 节点插槽

对于一些容器类组件和内置弹框API，一般都会用到slot来动态插入节点。注意，lit框架的模板字符串和web原生模板字符串、以及其他前端框架构建组件的字符串，以及实际渲染的节点实例都是不同的，这里框架存在类型判断和适配，但可能不全面，开发者也需要理解。

| 类型                   | 框架处理机制                                                 |
| ---------------------- | ------------------------------------------------------------ |
| html的Node实例         | 直接渲染进`<slot>`内                                         |
| web原生模板字符串      | 以标准模版字符串的形式进行渲染，写入`<slot>`内               |
| lit模板字符串          | 进行渲染后，写入`<slot>`                                     |
| 其他前端框架的html结构 | 跟框架对原生支持有关系，不能排除不能正常处理的可能，备选的建议先以document.createElement("div")的形式进行非渲染的实例化之后，再以Node实例的形式传入。详见《实例化范例》 |

##### 实例化范例

```JavaScript
let slotContent =   /*各类html结构*/
const div = document.createElement("div")   //div因为没有渲染到dom，因此也是不可见的
/**
    使用对应可用的方法进行实例化
    比如我的slotContent是lit模版字符串，则我可以使用render方法就行渲染
*/
render(slotContent , div); //将模板实例化成节点，挂在div上

//提取到slot变量上
const slot = div.firstElementChild

//然后使用slot传参
//...

//最后自定义组件拿到slot节点，最终预期渲染
```

##### 实例的转移和清除

原生的appendChild方法，以及部分框架的渲染方法，具有节点迁移作用，如lit的render(A.firstElementChild，B)会将节点从A转移到B。

在axue框架中，弹框API会造成节点转移，当close发生时，又会清除内部节点，这会导致最早的A.firstElementChild对象被销毁。因此，当出现这类问题是，标准的解决办法是，使用document.createElement("div")作为临时容器过渡。



### 数据方向和事件机制

##### 单向传递

傲雪基于lit，采用的是单向数据绑定，从父组件流向子组件，再借助事件反向冒泡回来。

如果你从父组件向组件传参，然后用户在子组件内部输入框，直接输入修改了`args.input.value`的值，这并不会自动触发父组件维护的args对象更新。需要通过监听事件的方式来实现反向数据流。

##### detail规范

e.detail用于挂载业务数据，这是lit扩展的事件标准，需要使用CustomEvent，傲雪继承该设计。

e.detail可能的值：

- 可能返回null、undefined，即回调不提供业务信息
- 一般不会返回空{}
- 有返回值则通过对象属性携带事件所返回的业务数据，具体结构详见各事件说明

##### data-key规范

一些场景很必要对事件元素设置key，比如input的value，select标签下option的key。在组件设计侧，是优先使用data-*来挂载内部元素索引，而非采用id、class、name。

key的主场景有二：

- 直接处理事件。比如选项框的change事件会借助detail对象直接返回来。
- 间接处理事件。同样选项框发生变化，业务上并一定需要立即处理，而是等所有label行都填完了，用户submit了，再统一根据key查询数据，借助类似`customElement.shadowRoot.querySelector('div[data-key="example"]');`直接拿到第一个匹配元素。

```JavaScript
//change事件
let detail
if(dataKey){
      detail={
            key:dataKey,      
            value:selectedValue
      }
}
const event = new CustomEvent('change', {
      composed: true,
      detail
});
this.dispatchEvent(event);
```

特别提醒：最好涉及key的传参，都用小写，web标准是支持大小写的，但一些 JavaScript 框架和库可能在处理`data-*`属性时对大小写敏感，为了确保兼容性，建议统一使用小写字母。

### onXXXX传参式回调

对于一些高级封装的组件，可能不得不使用传参式回调。

如果你在组件内部逻辑的运行时使用了lit框架的弹框API，又在弹框的插槽中插入了自己封装的组件，那么建议你使用传参式回调注入。因为弹框事件所在dom层级一定是高于你使用组件的父节点的，这会导致事件直接越过外围监听器，冒泡给了axue容器节点——>直接走出了document根，父节点监听不到。

这是框架设计带来的自然现象，理解他非常重要。

而传参式回调，走的是哈希索引，则不存在这样的边界，包治百病。



### 能力定位

框架的定位，是为了方便快速的组装常规界面，这意味着，框架核心不以全面的场景适应为目标，不会像老牌的组件库，为一个组件提供数十个的属性API。

- 比如labelInput，最多监听change事件，而不会去监听过程输入。如果有此需求，建议自行补充封装一下。
- 比如几乎所有的组件，都不提供样式类的属性，因为你完全可以使用内联style的最高优先级来实现目标。
