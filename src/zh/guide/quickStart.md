# 快速开始

##### 安装组件库

使用npmp、yarn等包管理在已有项目中引入axue框架

```JavaScript
npm i axue@latest --save
```

##### 全量导入

在使用组件之前，无具名导入，然后就可在页面任意地方自由使用组件
注意，在使用组件之前

```JavaScript
import "axue";     //axue只有区区200KB上下的尺寸，无需单个导入
```

##### 使用组件

在html中，或者在vue、react等前框框架模板中使用标签，像使用`<div>`那样

```HTML
<div>
    <axue-logo-close></axue-logo-close>
</div>
```

##### 使用API

在你喜欢的js逻辑中，调起Tip弹框

```js
import {showTip} from "axue";    //弹框需要具名导入
showTip.send('Hello, axue!');         //弹出tip，1.5s后消失
```
