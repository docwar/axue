# label-select

label系列组件之一，遵循一致的样式、边距、宽度体验，用于构建统一的风格

### 场景和形态

构建页面的信息条，左侧是label、右侧是选项框。 

![img](./assets/1704435684054-24.png)

### 使用方式

##### 定义属性参数

```JavaScript
let args={
    labelText:"姓名：",
    isShowFieldRequired:true,   //显示字段必填，但不做逻辑校验
    options:[
        {
            key:"xxxx-name",
            value:"傲雪",
        },
        {
            key:"yyyy-name",
            value:"青松",
        }
    ]
}
```

##### 定义事件属性

```JavaScript
function _change(e){
    console.log(e.detail)
}
```

##### 使用组件

```HTML
<axue-label-select .args=${args} @change=${_change}></axue-label-select>
```



### 属性项

| **内联属性**        | **说明**                         | **类型** | **默认值** |
| ------------------- | -------------------------------- | -------- | ---------- |
| label               | 图标url                          | string   | 内置图标   |
| isShowFieldRequired | 显示字段必填，但不做逻辑校验     | string   | true       |
| options             | 选项对象集                       | Object   | {}         |
| key                 | 遵循data-key规范，挂载在option上 | string   | null       |
| value               | 输入框的值，业务上必须要有的     | string   | ""         |
| disabled            | 是否禁用，一般用作选项标题       | bool     | false      |
| @change             | 选项变化回调                     | function | null       |
