# tab-x

### 场景和形态

一般位于窗口页面的上方、顶条的下方，不支持菜单分级，一般与布局组件结合，要么靠左，要么靠右。 

![img](./assets/1704435558902-10.png)

### 使用方式

##### 定义参数

```JavaScript
const aboutNode = document.getElementById("aboutAxue")
let  args = [{ 
    label: '首页', 
    onClick:function(){ console.log("测试tab导航，点击1")}
},
{
    label: '关于', 
    onClick:function(){}
}]
```

##### 使用组件

```HTML
<axue-tab-x .args=${args} ></axue-tab-x>
```

### 属性API

| **内联属性** | **说明**                         | **类型** | **默认值** |
| ------------ | -------------------------------- | -------- | ---------- |
| tab数组      | 对象数组，内部结构如下下面       | string   | ""         |
| label        | 菜单标题文本                     | string   | ""         |
| onClick      | 菜单单击事件回调，优先级不如slot | string   | 内置图标   |
|              |                                  |          |            |