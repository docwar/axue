# label-content-edit

label系列组件之一，遵循一致的样式、边距、宽度体验，用于构建统一的风格

### 场景和形态

构建详情页面的信息条，左侧是label、右侧是内容和编辑按钮，封装了弹出编辑体验。 

![image-20240108190305330](./assets/image-20240108190305330.png)

### 使用方式

##### 定义属性参数

与label-content参数完全一致，额外增加了编辑显示和点击回调注册

```JavaScript
let args={
    labelText:"姓名：",
    content:{
        key:"xxxx-name",
        value:"傲雪",
        type:"text"
    },
    editorText:"编辑",
    onUpdate(that,newValue){
        console.error("数据更新:",that,newValue)
    }
}
```

##### 使用组件

```HTML
<axue-label-content .args=${args} ></axue-label-content>
```



### 属性项

| **内联属性** | **说明**                                                 | **类型** | **默认值** |
| ------------ | -------------------------------------------------------- | -------- | ---------- |
| label        | 图标url                                                  | string   | 内置图标   |
| content      | 内容属性对象                                             | Object   | {}         |
| key          | 遵循data-key规范，挂载在div上                            | string   | null       |
| value        | 输入框的值                                               | string   | 内置图标   |
| type         | 内容类型，默认为文本，代表文本处理方式，属于框架通用规范 | string   | "text"     |
| editorText         | 编辑入口显示文本 | string   | "text"     |
| onUpdate | 如果没有定义onClickEdit，则走默认机制，此时数据更新时将回调，回传自定义组件对象本身，以及新的value值。 | function | null |
| onClickEdit  | 点击编辑入口文本的回调，如果没有，则使用默认机制弹出编辑框，更新用户设定，有，则表示需要自己封装编辑交互，将回传自定义组件对象本身。onClickEdit和onUpdate是互斥的 | function | null       |

