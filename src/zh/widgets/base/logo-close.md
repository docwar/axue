# logo-close

### 场景和形态

构建窗口页面的顶条，左侧是logo、名称，右侧是备注和关闭能力。 

![img](./assets/1704435481812-4.png)

### 使用方式

##### 至简默认

 只有左logo右close图标，没有名称和备注，默认的关闭事件处理——关闭父容器，无提示

```HTML
<axue-logo-close></axue-logo-close>
```

##### 定义属性参数

```HTML
<axue-logo-close .args=${{name:"启动器",marker:"第一步 连接想定配置"}} ></axue-logo-close>
```

##### 覆盖close事件响应

```HTML
<axue-logo-close  @clickClose=${this._close} ></axue-logo-close>
```

##### 定义logo双击事件响应

使用this._showDevTools来处理logo的双击事件，默认不处理

```HTML
<axue-logo-close @dbclickLogo=${this._showDevTools}></axue-logo-close>
```

### 属性项

| **内联属性** | **说明**              | **类型** | **默认值**                   |
| ------------ | --------------------- | -------- | ---------------------------- |
| logo         | 图标url               | string   | 内置图标                     |
| name         | 窗口左侧标题          | string   | ""                           |
| marker       | 窗口右侧注释          | string   | ""                           |
| close        | 关闭图标url           | string   | 内置图标                     |
| @dbclickLogo | logo图标双击事件回调  | function | null                         |
| @clickClose  | close图标单击事件回调 | function | 默认关闭当前标签的父容器null |

