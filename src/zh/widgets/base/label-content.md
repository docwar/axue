# label-content

label系列组件之一，遵循一致的样式、边距、宽度体验，用于构建统一的风格

### 场景和形态

构建详情页面的信息条，左侧是label、右侧是内容。 

![img](./assets/1704435704141-27.png)

### 使用方式

##### 定义属性参数

```JavaScript
let args={
    labelText:"姓名：",
    content:{
        key:"xxxx-name",
        value:"傲雪",
        type:"text"
    }
}
```

##### 使用组件

```HTML
<axue-label-content .args=${args} @change=${_change}></axue-label-content>
```



### 属性项

| **内联属性** | **说明**                                                 | **类型** | **默认值** |
| ------------ | -------------------------------------------------------- | -------- | ---------- |
| label        | 图标url                                                  | string   | 内置图标   |
| content      | 内容属性对象                                             | Object   | {}         |
| key          | 遵循data-key规范，挂载在div上                            | string   | null       |
| value        | 输入框的值                                               | string   | 内置图标   |
| type         | 内容类型，默认为文本，代表文本处理方式，属于框架通用规范 | string   | "text"     |