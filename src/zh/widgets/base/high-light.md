# ligh-light

### 场景和形态

占据100%宽度的高亮提示块，用于着重提示，嵌入在html文档结构中，并支持主动点击隐藏。一般放置在某个内容的上方。 

![img](./assets/1704435537188-7.png)

### 使用方式

##### 定义属性参数

```js
let args={
        type:"error",
        title:"错误调用",      
        content:"这样做是不对的，巴拉巴拉......",
        hasClose:true
    }
```

##### 使用组件

```HTML
<axue-high-light .args=${args} ></axue-high-light>
```



### 属性项

| **内联属性** | **说明**                              | **类型** | **默认值** |
| ------------ | ------------------------------------- | -------- | ---------- |
| type         | warning、info、error、success标准组合 | string   | "info"     |
| title        | 提示标题，优先与图标齐平              | string   | ""         |
| content      | 提示内容，如无将不占行位              | string   | ""         |
| hasClose     | 是否显示并可被用户×掉                 | bool     | false      |

