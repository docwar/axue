# menu-y2

### 场景和形态

竖向罗列形态的菜单，一般用于快速构建左边栏导航。只支持2级嵌套，你可以认为这是设计师的强迫症，我们不认可三级以上的集成体验设计。

![img](./assets/1704435606295-16.png)

### 使用方式

##### 定义参数

```JavaScript
const logo = new URL('../../assets/logo.svg', import.meta.url).href;
let  args = [
   {
        label:"主页",
        icon:logo,
        onClick:function (){
            //...
        }
    },{
        label:"订阅",
        menus: [{
            key:"123",
            enable:false,
            label:"订阅作者",
            switch:false,
        },{
            key:"234",
            label:"订阅傲雪",
            switch:true,
            onSwitch:function (switch){
                //...
            }
        }]
    }
]
```

##### 使用组件

```HTML
<axue-menu-y2 .args=${args} ></axue-menu-y2>
```



### 属性项

注意，传参是一个数组，成员是标准menu结构体

| **内联属性**  | **说明**                                                     | **类型**    | **默认值**   |
| ------------- | ------------------------------------------------------------ | ----------- | ------------ |
| menu数组      | 选项对象数组，内部结构如下                                   | array[menu] | ""           |
| label         | 菜单标题文本，长度自己去把握                                 | string      | ""           |
| tag           | 特别高亮标记，比如new高亮，会紧随label                       | string      | null         |
| key           | 菜单key，遵循data-key规范                                    | string      | null         |
| hover         | 以innerHtml的形式悬停显示，比如组件列表悬停显示预览图，或者tips说明，注意，由于最早版本子菜单依赖点击而非hover，默认hover相比click对用户的优先级更高，因此，hover会对menus配置造成拦截。 | htmlNode    | null         |
| icon          | 图标url，左侧是否显示图标                                    | string      | null         |
| switch        | 右侧是否支持开关交互，默认无配置表示不支持，配置值为false表示支持，默认选项是关闭，配置值为true表示 | bool        | null         |
| onClick       | 选项单击事件回调                                             | function    | null         |
| onSwitch      | 开关变更回调，在显示开关的情况拥有更高优先级，会屏蔽onClick事件 | function    | null         |
| menus         | 只有一级菜单可以继续内嵌一层menu数组，在有效指定menus的情况下，其优先级最高，会屏蔽事件回调，以及开关显示 | array[menu] | null         |
| menusPosition | 子菜单的显示位置： "auto"    智能计算坐标方位，一般是够用的，无需传参"top-left"    左上角"top-right"  右上角"bottom-left"  左下角"bottom-right"     右下角 | string      | null ="auto" |

##### 其他说明

- 如果一个菜单只有label，没有onClick、switch、menus等有效字段，将视为占位提示用途，呈现disabled效果
- 对二级菜单，menus字段被忽略之后，继续遵循上一条规则
- 除了base组件用于常显布局，menu的多数场景是通过右键、按钮点击、图像点击来触发显示的。我们还在业务域提供了两个高度封装的常用组件:`<button-menu-y2>`和`<avatar-menu-y2>`，其本质是省去了自己封装触发按钮样式和调用api的步骤。