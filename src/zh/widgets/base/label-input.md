# label-input

label系列组件之一，遵循一致的样式、边距、宽度体验，用于构建统一的风格

### 场景和形态

构建页面的信息条，左侧是label、右侧是输入框。 

![img](./assets/1704435621808-19.png)

### 使用方式

##### 定义属性参数

```JavaScript
let args={
    labelText:"姓名：",
    isShowFieldRequired:true,   //显示字段必填，但不做逻辑校验
    input:{
        key:"xxxx-name",
        value:"傲雪",
        placeHolder:"请输入",
        disabled:false,
    }
}
```

##### 定义事件监听

```JavaScript
function _change(e){
    console.log(e.detail)
}
```

##### 使用组件

```HTML
<axue-label-input .args=${args} @change=${_change}></axue-label-input>
```



### 属性项

| **内联属性**        | **说明**                         | **类型** | **默认值** |
| ------------------- | -------------------------------- | -------- | ---------- |
| label               | 图标url                          | string   | 内置图标   |
| isShowFieldRequired | 显示字段必填，但不做逻辑校验     | string   | true       |
| input               | 输入框属性对象                   | Object   | {}         |
| key                 | 遵循data-key规范，挂载在输入框上 | string   | null       |
| value               | 输入框的值                       | string   | 内置图标   |
| placeHolder         | 占位提示内容                     | bool     | false      |
| disabled            | 是否禁用                         | bool     | false      |
| @change             | 输入框值变化事件回调             | function | null       |

