# label-button-2

label系列组件之一，遵循一致的样式、边距、宽度体验，用于构建统一的风格

### 场景和形态

构建页面的按钮组合，左侧是label、右侧是取消确定按钮。 

![img](./assets/1704435753792-30.png)

### 使用方式

##### 定义属性参数

```JavaScript
let args={
      confirmText:"确认",
      cancelText:"cancel",
      // isShowCancel:false, 
      onConfirm:function(){},
      onCancel:function(){},
}
```

##### 使用组件

```HTML
<axue-label-button-2 .args=${args} ></axue-label-button-2>
```

### 属性项

| **内联属性** | **说明**                             | **类型** | **默认值** |
| ------------ | ------------------------------------ | -------- | ---------- |
| confirmText  | 确认按钮文本                         | string   | "确认"     |
| onConfirm    | 用户点击确认按钮的回调               | function | null       |
| isShowCancel | 是否显示取消按钮，默认只显示确认按钮 | bool     | false      |
| cancelText   | 取消按钮文本                         | string   | "取消"     |
| onCancel     | 用户点击取消按钮的回调               | function | null       |