# switch

### 形态和场景

类苹果设计风格的开关按钮。

![img](./assets/1704435369927-1.png)

### 使用方式

##### 使用组件

```HTML
<axue-switch @toggleSwitch=${(switchNode)=>{_toggleSwitch(switchNode)}></axue-switch>
```

##### 处理事件回调

```JavaScript
function _toggleSwitch(switchNode){
    if (switchNode.checked) {
      // 执行选中状态的操作
      console.log("开关已打开");
    } else {
      // 执行未选中状态的操作
      console.log("开关已关闭");
    }
}
```

##### 事件监听器

| **内联属性**  | **说明**     | **类型** | **默认值** |
| ------------- | ------------ | -------- | ---------- |
| @toggleSwitch | 开关切换回调 | function | null       |
|               |              |          |            |

