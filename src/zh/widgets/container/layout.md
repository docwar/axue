# 布局系列组件

提供了一组统一风格的行级布局组件

*  `<between>`元素两端对齐

* `<left>`元素左对齐 

*  `<right>`元素右对齐

### 场景和形态

快速组装行级排版，实现对齐布局，带一定内边距。

![img](./assets/1704435798790-36.png)

### 使用方式

##### 嵌入普通html标签

```HTML
<!--注意配置了边框-->
<axue-between border>
    <div>靠左</div>
    <div>靠右</div>
</axue-between>
```

##### 嵌套使用

```HTML
<axue-between >
    <div>靠左</div>
    <axue-left>
        <div>靠右</div>
        <div>靠右</div>
    </axue-left>
</axue-between>
```

##### 其他说明

- 无需传参，直接嵌入html结构即可
- 可以极简配置border属性
- 作为一个容器，默认带了基础容器的通用内边距
- 内部结构的宽度是自动分配的，如果需要针对性调整，可对嵌入的html结构进行内联样式自定义。
